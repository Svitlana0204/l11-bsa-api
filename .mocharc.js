const path = require("path");

module.exports = {
    require: [
        "ts-node/register",
        "source-map-support/register",
        path.join(__dirname, "tests", "plugins", "setup.ts"),
    ],
    timeout: 100000,
    parallel: false,
};
