import { expect } from "chai";
import { ArticleCommentController } from "../lib/controllers/article-comment.controller";
import { ArticleController } from "../lib/controllers/article.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { AuthorController } from "../lib/controllers/author.controller";
import { UserController } from "../lib/controllers/user.controller";
import ArticleWithAuthor from "../lib/models/articles/article-with-author";
import CreateArticle from "../lib/models/articles/create-article.model";
import AuthorSettings from "../lib/models/author/settings.model";
import CurrentUser from "../lib/models/user/current-user.model";
import { ApiRequest } from "../lib/request";
import inputs from "./data/inputs_testData.json";
import Mocks from "./data/mocks_testData";
import schemas from "./data/schemas_testData.json";

const auth = new AuthController();
const author = new AuthorController();
const user = new UserController();
const article = new ArticleController();
const articleComment = new ArticleCommentController();

describe("Chain 1", () => {
    let authorSettings: AuthorSettings;
    let userId: string;
    let articleId: string;
    let articleData: CreateArticle;
    let myArticles: ArticleWithAuthor[];
    let commentId: string;
    let commentText: string;
    let commentCreatedAt: string;
    let currentUser: CurrentUser;

    it("should log in", async () => {
        const response = await auth.login(inputs.input_authorLogin);

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(schemas.schema_loginResponse);

        ApiRequest.authToken = response.body.accessToken;
    });

    it("should save author settings", async () => {
        const infoResponse = await author.getSettings();

        authorSettings = Mocks.getAuthorSettings(infoResponse.body.id);
        const response = await author.setSettings(authorSettings);

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
    });

    it("should get current user", async () => {
        const response = await user.getCurrentUser();

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(schemas.schema_currentUser);

        expect(response.body).to.deep.include({
            email: inputs.input_authorLogin.email,
            emailVerified: true,
            avatar: authorSettings.avatar,
            role: {
                name: "AUTHOR",
            },
        });

        userId = response.body.id;
        currentUser = response.body;
    });

    it("should get author overview", async () => {
        const response = await author.getOverview(authorSettings.id);

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(schemas.schema_authorOverview);

        expect(response.body).to.deep.include({
            id: authorSettings.id,
            userId,
            firstName: authorSettings.firstName,
            lastName: authorSettings.lastName,
            avatar: authorSettings.avatar,
            biography: authorSettings.biography,
        });
    });

    it("should save article", async () => {
        articleData = Mocks.getCreateArticleData();
        const response = await article.saveArticle(articleData);

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(
            schemas.schema_articleWithAuthor
        );

        expect(response.body).to.deep.include({
            authorId: authorSettings.id,
            authorName:
                authorSettings.firstName + " " + authorSettings.lastName,
            ...articleData,
        });

        articleId = response.body.id;
    });

    it("should get my articles", async () => {
        const response = await article.getMyArticles();

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(schemas.schema_arrayOfArticles);

        expect(response.body).to.deep.include({
            id: articleId,
            authorId: authorSettings.id,
            authorName:
                authorSettings.firstName + " " + authorSettings.lastName,
            ...articleData,
        });

        myArticles = response.body;
    });

    it("should get created article", async () => {
        const response = await article.getOneArticle(articleId);

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(schemas.schema_fullArticle);

        expect(response.body).to.deep.include({
            id: null,
            ...articleData,
            author: {
                id: authorSettings.id,
                userId,
                avatar: authorSettings.avatar,
                name: authorSettings.firstName + " " + authorSettings.lastName,
                biography: authorSettings.biography,
                articles: myArticles,
            },
        });
    });

    it("should create comment", async () => {
        const createCommentData = Mocks.getCreateArticleCommentData(articleId);
        commentText = createCommentData.text;

        const response = await articleComment.createComment(createCommentData);

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(schemas.schema_articleComment);

        expect(response.body).to.deep.include({
            ...createCommentData,
            sourceId: articleId,
            user: {
                id: userId,
                email: currentUser.email,
                username: currentUser.nickname,
                role: "AUTHOR",
            },
        });

        commentId = response.body.id;
        commentCreatedAt = response.body.createdAt;
    });

    it("should get created comment", async () => {
        const response = await articleComment.getCommentsOf(articleId);

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;

        expect(response.body).to.be.jsonSchema(
            schemas.schema_arrayOfArticleComments
        );

        expect(response.body).to.deep.include({
            id: commentId,
            text: commentText,
            createdAt: commentCreatedAt,
            updatedAt: commentCreatedAt,
            articleId,
            sourceId: articleId,
            user: {
                id: userId,
                email: currentUser.email,
                username: currentUser.nickname,
                role: "AUTHOR",
            },
        });
    });
});
