import { expect } from "chai";
import { v4 } from "uuid";
import { AuthController } from "../lib/controllers/auth.controller";
import { FavoriteController } from "../lib/controllers/favorite.controller";
import { PathController } from "../lib/controllers/path.controller";
import { UserController } from "../lib/controllers/user.controller";
import Path from "../lib/models/paths/path.model";
import { ApiRequest } from "../lib/request";
import inputs from "./data/inputs_testData.json";
import schemas from "./data/schemas_testData.json";

const auth = new AuthController();
const user = new UserController();
const path = new PathController();
const favorite = new FavoriteController();

describe("Chain 2", () => {
    let randomPath: Path;
    let invalidPathId: string;

    it("should not log in with invalid password", async () => {
        const response = await auth.login(
            inputs.input_studentLoginWithInvalidPassword
        );

        expect(response).to.have.status(401);
        expect(response).to.have.normalTime;
    });

    it("should log in", async () => {
        const response = await auth.login(inputs.input_studentLogin);

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(schemas.schema_loginResponse);

        ApiRequest.authToken = response.body.accessToken;
    });

    it("should get current user", async () => {
        const response = await user.getCurrentUser();

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(schemas.schema_currentUser);

        expect(response.body).to.deep.include({
            email: inputs.input_studentLogin.email,
            emailVerified: true,
            role: {
                name: "USER",
            },
        });
    });

    it("should get all paths", async () => {
        const response = await path.getAllPaths();

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(schemas.schema_arrayOfPaths);

        const existingPathIds = response.body.map((path) => path.id);

        randomPath =
            response.body[Math.floor(Math.random() * response.body.length)];

        do {
            invalidPathId = v4();
        } while (existingPathIds.includes(invalidPathId));
    });

    it("should add path to favorites", async () => {
        const response = await favorite.changeFavorite(randomPath.id, "PATH");

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(schemas.schema_favorite);
        expect(response.body).to.be.true;
    });

    it("should check if path is favorite", async () => {
        const response = await favorite.isFavorite(randomPath.id, "PATH");

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(schemas.schema_favorite);
        expect(response.body).to.be.true;
    });

    it("should remove path from favorites", async () => {
        const response = await favorite.changeFavorite(randomPath.id, "PATH");

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(schemas.schema_favorite);
        expect(response.body).to.be.false;
    });

    it("should check if path is favorite", async () => {
        const response = await favorite.isFavorite(randomPath.id, "PATH");

        expect(response).to.have.status(200);
        expect(response).to.have.normalTime;
        expect(response.body).to.be.jsonSchema(schemas.schema_favorite);
        expect(response.body).to.be.false;
    });
});
