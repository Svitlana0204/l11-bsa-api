import CreateArticle from "../models/articles/create-article.model";
import { ApiRequest } from "../request";

export class ArticleController {
    saveArticle(data: CreateArticle) {
        return new ApiRequest().method("POST").url("article").body(data).send();
    }

    getMyArticles() {
        return new ApiRequest().method("GET").url("article/author").send();
    }

    getOneArticle(id: string) {
        return new ApiRequest().method("GET").url(`article/${id}`).send();
    }
}
