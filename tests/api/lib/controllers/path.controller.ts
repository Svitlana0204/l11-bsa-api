import { ApiRequest } from "../request";

export class PathController {
    getAllPaths() {
        return new ApiRequest().method("GET").url("paths/all").send();
    }
}
