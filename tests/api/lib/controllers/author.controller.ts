import AuthorSettings from "../models/author/settings.model";
import { ApiRequest } from "../request";

export class AuthorController {
    getSettings() {
        return new ApiRequest().method("GET").url("author").send();
    }

    setSettings(data: AuthorSettings) {
        return new ApiRequest().method("POST").url("author").body(data).send();
    }

    getOverview(id: string) {
        return new ApiRequest()
            .method("GET")
            .url(`author/overview/${id}`)
            .send();
    }
}
