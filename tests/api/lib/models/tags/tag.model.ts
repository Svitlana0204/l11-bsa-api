export default interface Tag {
    id: string;
    name: string;
    imageSrc: string;
}
