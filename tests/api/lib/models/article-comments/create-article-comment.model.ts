export default interface CreateArticleComment {
    articleId: string;
    text: string;
}
