import ArticleWithAuthor from "../articles/article-with-author";

export default interface AuthorWithArticles {
    articles: ArticleWithAuthor[];
    avatar: string;
    biography: string;
    id: string;
    name: string;
    userId: string;
}
