import Article from "../articles/article.model";
import Course from "../courses/course.model";

export default interface AuthorOverview {
    id: string;
    userId: string;
    firstName: string;
    lastName: string;
    avatar: string;
    biography: string;
    schoolName: string;
    schoolId: string;
    numberOfSubscribers: number;
    courses: Course[];
    articles: Article[];
    printFollowButton: boolean;
}
