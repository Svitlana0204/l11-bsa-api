export default interface CreateArticle {
    name: string;
    text: string;
    image: string;
}
