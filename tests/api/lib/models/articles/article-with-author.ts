export default interface ArticleWithAuthor {
    id: string;
    name: string;
    text: string;
    image: string;
    authorId: string;
    authorName: string;
}
