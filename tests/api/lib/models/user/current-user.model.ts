export default interface CurrentUser {
    avatar: string;
    email: string;
    emailVerified: boolean;
    id: string;
    nickname: string | null;
    role: {
        name: "AUTHOR" | "USER";
    };
}
